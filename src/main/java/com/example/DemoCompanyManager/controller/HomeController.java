package com.example.DemoCompanyManager.controller;


import com.example.DemoCompanyManager.model.Company;
import com.example.DemoCompanyManager.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Controller()
public class HomeController {


    @Autowired
    CompanyService companyService;


    @GetMapping("/showList")
    public ResponseEntity<?> getMenu( Company model, HttpServletRequest request){
        Random random = new Random();
        String[] name ={"VTI","AC","HR"};
        int i = random.nextInt(3);
        companyService.insertData(new Company("Code"+(int)(Math.random()*(500+1)),name[i],"Null","@"+name[i],"+84 "+(int)(Math.random()*(899999999)+100000000),"hmi hmi","haha","he he"));

        List<Company> list = companyService.getCompanyByCompanyCode("", "");


        request.setAttribute("list",list);
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    @DeleteMapping("/delete/{companyCode}")
    public ResponseEntity<?> deleteDatabase(@PathVariable String companyCode){
        Company company = companyService.getCompanyByPrimary(companyCode);
        companyService.deleteByCompanyCode(company);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    @DeleteMapping("/clear")
    public ResponseEntity<?> clearDatabase(){
        companyService.deleteAll();
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }


    @GetMapping("/home")
    public String getMenu(){
        return "Menu";
    }

    @PostMapping("/register")
    public ResponseEntity<Company> signUp(@RequestBody Company company){
        companyService.insertData(company);
        return ResponseEntity.status(HttpStatus.OK).body(company);
    }


    @PutMapping("/update")
    public ResponseEntity<Company> update(@RequestBody Company company) {
        companyService.insertData(company);
        return ResponseEntity.status(HttpStatus.OK).body(company);
    }

    @GetMapping("/search")
    public ResponseEntity<?> search(@RequestParam(name = "code", defaultValue = "") String companyCode, @RequestParam(name = "name", defaultValue = "") String name,
                                    @RequestParam(name = "email", defaultValue = "") String email, @RequestParam(name = "phone", defaultValue = "") String phone,
                                    @RequestParam(name = "status", defaultValue = "") String status, @RequestParam(name = "page",defaultValue = "0")int page) {
        Page<Company> companies = companyService.getCompanyByFields(companyCode,name,email,phone,status,PageRequest.of(page,5));
        return ResponseEntity.status(HttpStatus.OK).body(companies);
    }

    @GetMapping("/detail")
    public ResponseEntity<?> getDetail(@RequestParam(name = "code",defaultValue = "")String companyCode){
        Company company = companyService.getCompanyByPrimary(companyCode);
        return ResponseEntity.status(HttpStatus.OK).body(company);
    }
}
